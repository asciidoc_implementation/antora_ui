# -*- coding: utf-8 -*-
from os import mkdir, replace
import os.path as p
from shutil import rmtree
from subprocess import CalledProcessError, CompletedProcess, run, TimeoutExpired

_UI_BUNDLE: str = p.join(p.dirname(__file__), "ui_bundle")
_UI_BUILD: str = p.join(p.dirname(__file__), "build")


def run_bash_command(*values: str):
    try:
        _: CompletedProcess = run(values, check=True, timeout=30, shell=True)
    except TimeoutExpired as e:
        print(f"{e.__class__.__name__}, cmd = {e.cmd}, timeout = {e.timeout}")
        input("Press Enter to close the window ...")
    except CalledProcessError as e:
        print(f"{e.__class__.__name__}, cmd = {e.cmd}, return code = {e.returncode}")
        input("Press Enter to close the window ...")
    else:
        print(f"Command {_.args} is completed")


if __name__ == '__main__':
    if not p.exists(p.join(p.dirname(p.abspath(__file__)), "node_modules")):
        run_bash_command("npm", "ci")
    run_bash_command("npx", "gulp", "bundle")
    if p.exists(_UI_BUNDLE):
        rmtree(_UI_BUNDLE)
    mkdir(_UI_BUNDLE)
    _old: str = p.join(_UI_BUILD, "build/ui-bundle.zip")
    _new: str = p.join(_UI_BUNDLE, "build/ui-bundle.zip")
    replace(_old, _new)
